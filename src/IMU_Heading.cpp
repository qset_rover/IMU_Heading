/* 

PUT A DESCRIPTION HERE

*/

#include <iostream>
#include "ros/ros.h"
#include "qset_msgs/wheelSpeed.h"
#include "sensor_msgs/JointState.h"
#include "qset_msgs/sabertooth.h"
#include "qset_msgs/EncoderDecimatedSpeed.h"
#include <std_msgs/Float64.h>
#include "std_msgs/Float32.h"
#include "diagnostic_msgs/DiagnosticArray.h"
#include <math.h>
#include "geometry_msgs/Twist.h"
#include <functional>
#include <dynamic_reconfigure/server.h>
#include "dynamic_reconfigure/server.h"
#include <cmath>

//#include "wheel_control/wheel_controlConfig.h" // why is this here?

// FIND OUT HOW TO INCLUDE THIS
//#include "tf/transformations.h"
#include "tf/LinearMath/Quaternion.h"

typedef unsigned char byte;

using namespace std;

/*--------------------Variable Defenitions--------------------*/

// Set points are taken in from the outsde world and used to set target speeds for the control
float setPoints[2] = {0, 0};
/*
 	0 --> Left
	1 --> Right
*/

// We've got different control modes for the wheels
int wheelMode = 1;

const int PIDMode = 0;// --> normal PID control 
const int voltageMode = 1;// --> straight voltage control ie. motor encoders are fucked 

// The range that is written to the sabertooth is 0 to 127 where 0 is full backwards, 64 is stopped, 127 is full forward.
uint16_t motorSpeed[4] = {64, 64, 64, 64};
const int motor_FL = 0; //Front Left
const int motor_FR = 1; //Front Right
const int motor_BL = 2; //Back Left
const int motor_BR = 3; //Back Right

// Read the sabertooth documentation if you want to understand this
uint16_t motorAddress[][2] = {{130, 6},{128, 7},{128, 6},{130, 7}};
/*
	0 --> Front Left
	1 --> Front Right
	2 --> Back Left
	3 --> Back Right
*/

const float stopCommand = 64.0;

// Control gains
float kp = 0.12;
float ki = 0.0012;
float kd = 0.00001;
// Variables used in normalControl(). Declared here so that they don't get redeclared everytime the function runs.

float sp; //motor setpoint
float error;
float encSpeed;
float throttleVal;
int motorVal;
int maxSpeed = 127;
int minSpeed = 0;
// these are arrays so one value is stored for each wheel 
float cumError[4]; 
float lastError[4]; 
float derError[4]; 

float speedMultipliers[4] = {1,-1,1,-1};

float euler_orientation[3] = {0,0,0};
float heading = 0;
int rt;
float speed;

float rotation_setpoint = 0;
float drive_setpoint = 0;

/*--------------------Function Defenitions--------------------*/

void IMUCallback(const std_msgs::Float32 msg){
    //Get IMU heading
    heading = msg.data;
}


//CREATE A MESSAGE FILE TO DENOTE A ROTATION TYPE()
// int rotation_type = 0 or 1 (0 is position, 1 is velocity)
// float magnitude = any position or velocity
void cmdCallback(const geometry_msgs::Twist msg){

	drive_setpoint = msg.linear.x;
	rotation_setpoint = msg.angular.z;

}//end cmdCallback

//This does the thing
void IMUControl() {

	if(drive_setpoint != 0 && rotation_setpoint != 0) //Linear and angular command.
	{
		ROS_INFO("We're too monkey brained to do linear plus angular commands right now, stick to one at a time :).");
	}
	else if (drive_setpoint != 0)//Linear command
	{
		if(drive_setpoint >  0) //Move forward command
		{
			setPoints[0] = 0.5;
			setPoints[1] = 0.5;
		}
		else //Move backward command
		{
			setPoints[0] = -0.5;
			setPoints[1] = -0.5;
		}
	}
	else //Angular command
	{
		//okay +90 and -270 degrees are mathematically identical but you don't want to do a longer turn than you have to so do some mathy stuff
		
		//this gives the shortest angle turn required to get to the setpoint.
		float angle_err = rotation_setpoint - heading;
		if(abs(angle_err) > 180)
		{
			if(angle_err>=0)
			{
				angle_err -= 360;
			}
			else
			{
				angle_err += 360;
			}
		}

		//this calculates wheel speed setpoints using a simple P controller with a minimum speed so it doesn't asymptotically approach the setpoint.
		float max_turn_speed = 0.5; //Max happens when the rover is 180 deg away from setpoint
		float min_turn_speed = 0.1; //Always turns this fast until the setpoint is reached
		float deadband = 2;
		float rot_speed = (abs(angle_err)*180)*max_turn_speed;

		if(abs(angle_err) <= deadband)
		{
			setPoints[0] = 0;
			setPoints[1] = 0;
		}
		else
		{
			if(rot_speed < min_turn_speed)//minimum turning speed
			{
				if(angle_err < 0)//Turning left
				{
					setPoints[0] = -1*min_turn_speed;
					setPoints[1] = min_turn_speed;
				}
				else//Turning right
				{
					setPoints[1] = -1*min_turn_speed;
					setPoints[0] = min_turn_speed;
				}
			}
			else
			{
				if(angle_err < 0)//Turning left
				{
					setPoints[0] = -1*rot_speed;
					setPoints[1] = rot_speed;
				}
				else//Turning right
				{
					setPoints[1] = -1*rot_speed;
					setPoints[0] = rot_speed;
				}
			}
		}
	}
	motorSpeed[motor_FL] = setPoints[0]*63.0*speedMultipliers[motor_FL] + stopCommand;
	motorSpeed[motor_FR] = -setPoints[1]*63.0*speedMultipliers[motor_FR] + stopCommand;
	motorSpeed[motor_BL] = setPoints[0]*63.0*speedMultipliers[motor_BL] + stopCommand;
	motorSpeed[motor_BR] = -setPoints[1]*63.0*speedMultipliers[motor_BR] + stopCommand;
}//end IMUControl


void eStop() {
	for(int i; i<4; i++)
		motorSpeed[i] = stopCommand; 
}//end eStop

int main(int argc, char **argv) {

	//set up the ROS node
	ros::init(argc, argv, "imu_control");
	ros::NodeHandle n;
	ros::Subscriber cmdSub = n.subscribe("/cmd_vel", 4, cmdCallback);
	ros::Publisher pub = n.advertise<qset_msgs::sabertooth>("/wheels/sabertooth", 1);

    ros::Rate loop_rate(80);
	ros::Rate send_rate(20);

    ROS_INFO("IMU Control node ready");

	//the whole node loops on this while loop
	while (ros::ok()) {
	  wheelMode = voltageMode; //remove this if you want PID mode --------------------------------------
	  for (int i = 0; i < 4; i++) {
            if (wheelMode == voltageMode)
              IMUControl();
            else
              eStop();

	  //EncFailureHandler();

            	//sending the sabertooth message
            	qset_msgs::sabertooth msg;

            	msg.data = motorSpeed[i];
            	msg.address = motorAddress[i][0];
            	msg.command = motorAddress[i][1];
            	ROS_INFO("Sending Speed [%d] to motor [%d] with multiplier [%f].\n", motorSpeed[i], i, speedMultipliers[i]);
            	pub.publish(msg);
	    ros::spinOnce();
            send_rate.sleep();
	  }
	loop_rate.sleep();
	}

	return 0;

}//end main
